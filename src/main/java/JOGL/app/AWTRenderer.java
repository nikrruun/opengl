package JOGL.app;

import static com.jogamp.opengl.GL.*;
import static com.jogamp.opengl.GL2ES1.GL_PERSPECTIVE_CORRECTION_HINT;
import static com.jogamp.opengl.fixedfunc.GLLightingFunc.GL_SMOOTH;
import static com.jogamp.opengl.fixedfunc.GLMatrixFunc.GL_MODELVIEW;
import static com.jogamp.opengl.fixedfunc.GLMatrixFunc.GL_PROJECTION;
import static com.jogamp.newt.event.KeyEvent.*;

import java.awt.AWTException;
import java.awt.Point;
import java.awt.Robot;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseMotionListener;
import java.awt.image.BufferedImage;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.swing.JFrame;

import com.jogamp.opengl.GL2;
import com.jogamp.opengl.GLAutoDrawable;
import com.jogamp.opengl.GLEventListener;
import com.jogamp.opengl.GLException;
import com.jogamp.opengl.GLProfile;
import com.jogamp.opengl.awt.GLCanvas;
import com.jogamp.opengl.glu.GLU;
import com.jogamp.opengl.util.texture.Texture;
import com.jogamp.opengl.util.texture.awt.AWTTextureIO;

public class AWTRenderer implements GLEventListener {
	private GLU glu; // for the GL Utility

	// The world
	private Sector floor;
	private Sector walls;

	private Texture floorTexture;
	private String floorTexturePath = "../../images/grass.png";
	private Texture wallsTexture;
	private String wallsTexturePath = "../../images/fence.png";

	private boolean blendingEnabled; // Blending ON/OFF

	// x and z position of the player, y is 0
	private float posX = 0;
	private float posZ = 0;
	private float posY = 0.3f;
	private float headingY = 0f; // heading of player, about y-axis
	private float lookUpAngle = 0.0f;
	private float speed = 0.1f;
	private int width;
	private int height;

	private float walkBias = 0;
	private float walkBiasAngle = 0;
	private boolean noclip = false;
	private boolean isLeftDown = false, isRightDown = false, isUpDown = false, isDownDown = false;
	private GLCanvas root;
	private Robot mouseRobot;
	private JFrame frame;

	public AWTRenderer(GLCanvas window, JFrame frame) {
		this.frame = frame;
		root = window;
		root.addKeyListener(new WorldKeyAdapter());
		root.addMouseMotionListener(new WorldMouseAdapter());
		try {
			mouseRobot = new Robot();
		} catch (AWTException e) {
			e.printStackTrace();
		}
	}

	class WorldMouseAdapter implements MouseMotionListener {

		public void mouseMoved(MouseEvent e) {
			int centerX = width / 2, centerY = height / 2;
			Point p = e.getLocationOnScreen();
			headingY += (centerX - p.x) / 2.0f;
			lookUpAngle -= (centerY - p.y) / 2.0f;
			if(lookUpAngle<-70) lookUpAngle = -70;
			if(lookUpAngle>90) lookUpAngle = 90;
			mouseRobot.mouseMove(centerX, centerY);
		}

		public void mouseDragged(MouseEvent e) {

		}
	}

	class WorldKeyAdapter implements KeyListener {

		public void keyPressed(KeyEvent e) {
			switch (e.getKeyCode()) {
			case VK_A:
			case VK_LEFT:
				isLeftDown = true;
				break;
			case VK_D:
			case VK_RIGHT:
				isRightDown = true;
				break;
			case VK_W:
			case VK_UP:
				isUpDown = true;
				break;
			case VK_S:
			case VK_DOWN:
				isDownDown = true;
				break;
			case VK_B:
				blendingEnabled = !blendingEnabled;
				break;
			case VK_SPACE:
				break;
			case VK_N:
				noclip = !noclip;
				break;
			case VK_ESCAPE:
				System.exit(0);
				break;
			case VK_F:
				if (frame.getExtendedState() == JFrame.NORMAL)
					frame.setExtendedState(JFrame.MAXIMIZED_BOTH);
				else
					frame.setExtendedState(JFrame.NORMAL);
				mouseRobot.mouseMove(frame.getWidth() / 2, frame.getHeight() / 2);
				break;
			}
		}

		public void keyReleased(KeyEvent e) {
			switch (e.getKeyCode()) {
			case VK_A:
			case VK_LEFT:
				isLeftDown = false;
				break;
			case VK_D:
			case VK_RIGHT:
				isRightDown = false;
				break;
			case VK_W:
			case VK_UP:
				isUpDown = false;
				break;
			case VK_S:
			case VK_DOWN:
				isDownDown = false;
				break;
			case VK_ESCAPE:
				root.destroy();
				break;
			}
		}

		public void keyTyped(KeyEvent e) {

		}

	}

	private void setupWorld() {
		floor = Sector.importSector("../../models/floor.txt", floorTexture);
		walls = Sector.importSector("../../models/walls.txt", wallsTexture);

	}

	public void init(GLAutoDrawable drawable) {
		GL2 gl = drawable.getGL().getGL2(); // get the OpenGL graphics context
		glu = new GLU(); // get GL Utilities
		gl.glClearColor(0.0f, 0.0f, 0.0f, 0.0f); // set background (clear) color
		gl.glClearDepth(1.0f); // set clear depth value to farthest
		gl.glEnable(GL_DEPTH_TEST); // enables depth testing
		gl.glDepthFunc(GL_LEQUAL); // the type of depth test to do
		gl.glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST); // best
		// perspective
		// correction
		gl.glShadeModel(GL_SMOOTH); // blends colors nicely, and smoothes out
		// lighting

		// Load Textures
		loadTextures(gl);

		// Read the world
		setupWorld();
	}

	private void loadTextures(GL2 gl) {
		// Load the texture image
		try {
			// Use URL so that can read from JAR and disk file.

			BufferedImage image = ImageIO.read(this.getClass().getResource(wallsTexturePath));

			wallsTexture = AWTTextureIO.newTexture(GLProfile.getDefault(), image, false);
			gl.glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
			gl.glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
			gl.glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
			gl.glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);

			image = ImageIO.read(this.getClass().getResource(floorTexturePath));
			floorTexture = AWTTextureIO.newTexture(GLProfile.getDefault(), image, false);
			gl.glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
			gl.glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
			gl.glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
			gl.glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);

			//
			// textures[1] = AWTTextureIO.newTexture(GLProfile.getDefault(),
			// image, false);
			// // Linear filter is more compute-intensive
			// // Use linear filter if image is larger than the original texture
			// gl.glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER,
			// GL_LINEAR);
			// // Use linear filter if image is smaller than the original
			// texture
			// gl.glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER,
			// GL_LINEAR);
			// gl.glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
			// gl.glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
			//
			// textures[2] = AWTTextureIO.newTexture(GLProfile.getDefault(),
			// image, true); // mipmap is true
			// // Use mipmap filter is the image is smaller than the texture
			// gl.glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER,
			// GL_LINEAR);
			// gl.glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER,
			// GL_LINEAR_MIPMAP_NEAREST);
			// gl.glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
			// gl.glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
		} catch (GLException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

		// // Enable the texture
		gl.glEnable(GL_TEXTURE_2D);

		// Blending control
		gl.glColor4f(1.0f, 1.0f, 1.0f, 0.9f); // Brightness with alpha
		// Blending function For translucency based On source alpha value
		gl.glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

	}

	public void display(GLAutoDrawable drawable) {
		GL2 gl = drawable.getGL().getGL2(); // get the OpenGL 2 graphics context
		gl.glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT); // clear color
		// and depth
		// buffers
		gl.glLoadIdentity(); // reset the model-view matrix
		gl.glClearColor(0f, 0.5f, 0.8f, 1f);
		// Blending control
		if (blendingEnabled) {
			gl.glEnable(GL_BLEND); // Turn Blending On
			gl.glDisable(GL_DEPTH_TEST); // Turn Depth Testing Off
		} else {
			gl.glDisable(GL_BLEND); // Turn Blending Off
			gl.glEnable(GL_DEPTH_TEST); // Turn Depth Testing On
		}
		move();
		// Rotate up and down to look up and down
		gl.glRotatef(lookUpAngle, 1.0f, 0, 0);

		// Player at headingY. Rotate the scene by -headingY instead (add 360 to
		// get a positive angle)
		gl.glRotatef(360.0f - headingY, 0, 1.0f, 0);
		
		// Player is at (posX, 0, posZ). Translate the scene to (-posX, 0,
		// -posZ) instead.
		gl.glTranslatef(-posX, -walkBias - 0.25f - posY, -posZ);

		floor.draw(gl);
		gl.glPushMatrix();
		gl.glTranslatef(0, -0.12f, 0);
		gl.glEnable(GL_BLEND);
		walls.draw(gl);
		gl.glDisable(GL_BLEND);
		gl.glPopMatrix();
	}

	private void move() {
		if (isLeftDown) {
			posX -= (float) Math.cos(Math.toRadians(headingY)) * speed;
			posZ += (float) Math.sin(Math.toRadians(headingY)) * speed;
		}
		if (isRightDown) {
			posX += (float) Math.cos(Math.toRadians(headingY)) * speed;
			posZ -= (float) Math.sin(Math.toRadians(headingY)) * speed;
		}
		if (isUpDown) {
			// Player move in, posX and posZ become smaller
			posX -= (float) Math.sin(Math.toRadians(headingY)) * speed;
			posZ -= (float) Math.cos(Math.toRadians(headingY)) * speed;

			walkBiasAngle = (walkBiasAngle >= 359.0f) ? 0.0f : walkBiasAngle + 10.0f;
			walkBias = (float) Math.sin(Math.toRadians(walkBiasAngle)) / 20.0f;
		}
		if (isDownDown) {
			// Player move out, posX and posZ become bigger
			posX += (float) Math.sin(Math.toRadians(headingY)) * speed;
			posZ += (float) Math.cos(Math.toRadians(headingY)) * speed;
			walkBiasAngle = (walkBiasAngle <= 1.0f) ? 359.0f : walkBiasAngle - 10.0f;
			walkBias = (float) Math.sin(Math.toRadians(walkBiasAngle)) / 20.0f;
		}
		if (!noclip) {
			if (posX > 9.5)
				posX = 9.5f;
			if (posX < -9.5)
				posX = -9.5f;
			if (posZ > 9.5)
				posZ = 9.5f;
			if (posZ < -9.5)
				posZ = -9.5f;
		}
	}

	public void reshape(GLAutoDrawable drawable, int x, int y, int width, int height) {
		GL2 gl = drawable.getGL().getGL2(); // get the OpenGL 2 graphics context
		System.out.println("width: " + width + ", height:" + height);
		if (height == 0)
			height = 1; // prevent divide by zero
		float aspect = (float) width / height;
		this.width = width;
		this.height = height;
		// Set the view port (display area) to cover the entire window
		gl.glViewport(0, 0, width, height);

		// Setup perspective projection, with aspect ratio matches viewport
		gl.glMatrixMode(GL_PROJECTION); // choose projection matrix
		gl.glLoadIdentity(); // reset projection matrix
		glu.gluPerspective(45.0, aspect, 0.1, 100.0); // fovy, aspect, zNear,
		// zFar

		// Enable the model-view transform
		gl.glMatrixMode(GL_MODELVIEW);
		gl.glLoadIdentity(); // reset
	}

	public void dispose(GLAutoDrawable drawable) {
	}

}
