package JOGL.app;
import javax.swing.JFrame;

import com.jogamp.opengl.GLCapabilities;
import com.jogamp.opengl.GLProfile;
import com.jogamp.opengl.awt.GLCanvas;
import com.jogamp.opengl.util.FPSAnimator;

public class App {
	public static void main(String[] args) {
		// setup OpenGL Version 2
		GLProfile profile = GLProfile.getDefault();
		GLCapabilities capabilities = new GLCapabilities(profile);
		final JFrame frame = new JFrame("Hello World");
		frame.setUndecorated(true);
		final GLCanvas glcanvas = new GLCanvas(capabilities);
		AWTRenderer renderer = new AWTRenderer(glcanvas,frame);
		glcanvas.addGLEventListener(renderer);
		glcanvas.setFocusable(true);
		glcanvas.requestFocus();
		glcanvas.setSize(1024, 768);
		final FPSAnimator animator = new FPSAnimator(glcanvas, 60);
		animator.start();
		frame.getContentPane().add(glcanvas);
		frame.setSize(frame.getContentPane().getPreferredSize());
		frame.setLocation(100, 100);
		frame.setVisible(true);
	}
}