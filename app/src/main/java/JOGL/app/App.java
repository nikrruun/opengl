package JOGL.app;

import static java.awt.event.KeyEvent.*;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

import javax.swing.JFrame;

import com.jogamp.opengl.GLCapabilities;
import com.jogamp.opengl.GLProfile;
import com.jogamp.opengl.awt.GLCanvas;
import com.jogamp.opengl.util.FPSAnimator;

public class App {
	public static void main(String[] args) {
		// setup OpenGL Version 2
		GLProfile profile = GLProfile.getDefault();
		GLCapabilities capabilities = new GLCapabilities(profile);
		final JFrame frame = new JFrame("Hello World");
		frame.setUndecorated(true);
		// The canvas is the widget that's drawn in the JFrame
		final GLCanvas glcanvas = new GLCanvas(capabilities);
		AWTRenderer renderer = new AWTRenderer(glcanvas,frame);
		glcanvas.addGLEventListener(renderer);
		glcanvas.setFocusable(true);
		glcanvas.requestFocus();
		glcanvas.setSize(1024, 768);
		final FPSAnimator animator = new FPSAnimator(glcanvas, 60);
		animator.start();
		frame.getContentPane().add(glcanvas);

//		frame.addWindowListener(new WindowAdapter() {
//			public void windowClosing(WindowEvent ev) {
//				System.out.println("Closing");
//				animator.stop();
//				frame.remove(glcanvas);
//				frame.dispose();
//				System.exit(0);
//			}
//		});

		frame.setSize(frame.getContentPane().getPreferredSize());
		frame.setLocation(100, 100);
		frame.setVisible(true);
	}
}

//
// public static void main(String[] args) {
// // Create the OpenGL rendering canvas
// GLCanvas canvas = new GLCanvas(); // heavy-weight GLCanvas
// canvas.setPreferredSize(new Dimension(CANVAS_WIDTH, CANVAS_HEIGHT));
// Renderer renderer = new Renderer();
// canvas.addGLEventListener(renderer);
//
// // For Handling KeyEvents
// canvas.addKeyListener(renderer);
// canvas.setFocusable(true);
// canvas.requestFocus();
//
// // Create an animator that drives canvas' display() at the specified FPS.
// final FPSAnimator animator = new FPSAnimator(canvas, FPS, true);
//
// // Create the top-level container frame
// final JFrame frame = new JFrame(); // Swing's JFrame or AWT's Frame
// frame.getContentPane().add(canvas);
// frame.addWindowListener(new WindowAdapter() {
//
// public void windowClosing(WindowEvent e) {
// // Use a dedicate thread to run the stop() to ensure that the
// // animator stops before program exits.
// new Thread() {
//
// public void run() {
// animator.stop(); // stop the animator loop
// System.exit(0);
// }
// }.start();
// }
// });
// frame.setTitle(TITLE);
// frame.pack();
// frame.setVisible(true);
// animator.start();
// }
