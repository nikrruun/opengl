package JOGL.app;

import static com.jogamp.opengl.GL.GL_TRIANGLES;

import java.io.BufferedReader;
import java.io.InputStreamReader;

import com.jogamp.opengl.GL2;
import com.jogamp.opengl.util.texture.Texture;
import com.jogamp.opengl.util.texture.TextureCoords;

public class Sector {
	private final Triangle[] triangles;
	private final Texture texture;
	private final float textureTop, textureBottom;

	public Sector(int numTriangles, Texture texture) {
		this.texture = texture;
		TextureCoords textureCoords = texture.getImageTexCoords();
		textureTop = textureCoords.top();
		textureBottom = textureCoords.bottom();
		triangles = new Triangle[numTriangles];
		for (int i = 0; i < numTriangles; i++) {
			triangles[i] = new Triangle();
		}
	}

	public void draw(GL2 gl) {
		texture.bind(gl);
		// Process each triangle
		for (int i = 0; i < triangles.length; i++) {
			gl.glBegin(GL_TRIANGLES);
			gl.glNormal3f(0.0f, 0.0f, 1.0f); // Normal pointing out of screen

			// need to flip the image
			float textureHeight = textureTop - textureBottom;
			float u, v;

			u = triangles[i].vertices[0].u;
			v = triangles[i].vertices[0].v * textureHeight - textureBottom;
			gl.glTexCoord2f(u, v);
			gl.glVertex3f(triangles[i].vertices[0].x, triangles[i].vertices[0].y, triangles[i].vertices[0].z);

			u = triangles[i].vertices[1].u;
			v = triangles[i].vertices[1].v * textureHeight - textureBottom;
			gl.glTexCoord2f(u, v);
			gl.glVertex3f(triangles[i].vertices[1].x, triangles[i].vertices[1].y, triangles[i].vertices[1].z);

			u = triangles[i].vertices[2].u;
			v = triangles[i].vertices[2].v * textureHeight - textureBottom;
			gl.glTexCoord2f(u, v);
			gl.glVertex3f(triangles[i].vertices[2].x, triangles[i].vertices[2].y, triangles[i].vertices[2].z);

			gl.glEnd();
		}
	}

	public static Sector importSector(String path, Texture texture) {
		BufferedReader in = null;
		Sector sector = null;
		try {
			in = new BufferedReader(new InputStreamReader(Sector.class.getResourceAsStream(path)));
			String line = null;
			while ((line = in.readLine()) != null) {
				if (line.trim().length() == 0 || line.trim().startsWith("//"))
					continue;
				if (line.startsWith("NUMPOLLIES")) {
					int numTriangles;

					numTriangles = Integer.parseInt(line.substring(line.indexOf("NUMPOLLIES") + "NUMPOLLIES".length() + 1));
					sector = new Sector(numTriangles, texture);
					break;
				}
			}

			for (int i = 0; i < sector.triangles.length; i++) {
				for (int vert = 0; vert < 3; vert++) {
					while ((line = in.readLine()) != null) {
						if (line.trim().length() == 0 || line.trim().startsWith("//"))
							continue;
						break;
					}
					if (line != null && line.length() > 5) {
						line = line.trim().replaceAll("( ){2,}", " ");
						String[] s = line.split(" ");
						sector.triangles[i].vertices[vert].x = Float.parseFloat(s[0]);
						sector.triangles[i].vertices[vert].y = Float.parseFloat(s[1]);
						sector.triangles[i].vertices[vert].z = Float.parseFloat(s[2]);
						sector.triangles[i].vertices[vert].u = Float.parseFloat(s[3]);
						sector.triangles[i].vertices[vert].v = Float.parseFloat(s[4]);
					}
				}
			}
			in.close();

		} catch (Exception e) {
			e.printStackTrace();
		}
		return sector;
	}
}

class Triangle {
	Vertex[] vertices = new Vertex[3];

	public Triangle() {
		vertices[0] = new Vertex();
		vertices[1] = new Vertex();
		vertices[2] = new Vertex();
	}
}

class Vertex {
	float x, y, z; // 3D x,y,z location
	float u, v; // 2D texture coordinates

	public String toString() {
		return "(" + x + "," + y + "," + z + ")" + "(" + u + "," + v + ")";
	}
}