package JOGL.app;

import com.jogamp.newt.event.WindowAdapter;
import com.jogamp.newt.event.WindowEvent;
import com.jogamp.newt.opengl.GLWindow;
import com.jogamp.opengl.GLCapabilities;
import com.jogamp.opengl.GLProfile;
import com.jogamp.opengl.util.FPSAnimator;

public class NewtAPP {

	public static void main(String[] args) {
		GLProfile profile = GLProfile.getDefault();
		GLCapabilities capabilities = new GLCapabilities(profile);
		final GLWindow glcanvas = GLWindow.create(capabilities);
		Renderer renderer = new Renderer(glcanvas);
		glcanvas.addGLEventListener(renderer);
		glcanvas.setSize(1024, 768);
		glcanvas.requestFocus();
		final FPSAnimator animator = new FPSAnimator(glcanvas, 60);
		glcanvas.addWindowListener(new WindowAdapter() {
			 @Override
	            public void windowDestroyNotify(WindowEvent arg0) {
	                new Thread() {
	                    @Override
	                    public void run() {
	                        if (animator.isStarted())
	                            animator.stop();    // stop the animator loop
	                        glcanvas.destroy();
	                        System.exit(0);
	                    }
	                }.start();
	            }
		});
		animator.start();
		
		glcanvas.setVisible(true);
	}

}
